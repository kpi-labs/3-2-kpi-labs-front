export default {
  namespaced: true,
  state: {
    isAlertShown: false,
    alertText: null,
    isLoading: false
  },
  mutations: {
    closeAlert(state) {
      state.isAlertShown = false;
      state.alertText = null;
    },
    showAlert(state, text) {
      state.isAlertShown = true;
      state.alertText = text;
    },
    startLoading(state) {
      state.isLoading = true;
    },
    stopLoading(state) {
      state.isLoading = false;
    }
  },
  actions: {
    showAlert({ commit }, text) {
      commit('showAlert', text);
    },
    closeAlert({ commit }) {
      commit('closeAlert');
    },
    startLoading({ commit }) {
      console.log('start');

      commit('startLoading');
    },
    stopLoading({ commit }) {
      console.log('stop');
      commit('stopLoading');
    }
  }
};
