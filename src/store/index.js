import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import auth from './auth.store';
import criminalArticles from './criminalArticles.store';
import personalPccos from './personalPccos.store';
import ui from './ui.store';
import logs from './logs.store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    logs,
    ui,
    personalPccos,
    criminalArticles
  },
  state: {},
  mutations: {},
  actions: {},
  plugins: [createPersistedState({ paths: ['auth'] })],
});
