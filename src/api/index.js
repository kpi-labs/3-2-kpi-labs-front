import PersonalPccosRepository from './personalPccosRepository';
import CriminalArticlesRepository from './criminalArticlesRepository';
import AuthRepository from './authRepository';
import LogsRepository from './logsRepository';

export default {
  LogsRepository,
  AuthRepository,
  PersonalPccosRepository,
  CriminalArticlesRepository
};
