import all from '../api';
import { normalizeServerErrorResponse } from '../helpers';

const { LogsRepository } = all;

export default {
  namespaced: true,
  state: {
    logs: []
  },
  mutations: {
    setLogs(state, logs) {
      state.logs = logs;
    }
  },
  actions: {
    async loadAllByPcco({ commit }, pccoId) {
      commit('ui/startLoading', null, { root: true });
      try {
        const { data } = await LogsRepository.list({
          affectedRecordId: pccoId,
          limit: 999
        });
        commit('setLogs', data.data);
      } catch (err) {
        commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
      }
      commit('ui/stopLoading', null, { root: true });
    }
  }
};
