import all from '../api';

const { AuthRepository } = all;

export default {
  namespaced: true,
  getters: {
    isLogged: state => !!state.accessToken
  },
  state: {
    accessToken: null,
    refreshToken: null
  },
  mutations: {
    setAccessToken(state, token) {
      state.accessToken = token;
    },
    setRefreshToken(state, token) {
      state.refreshToken = token;
    }
  },
  actions: {
    logout({ commit }) {
      commit('setAccessToken', null);
      commit('setRefreshToken', null);
    },
    async login({ commit }, { email, password }) {
      try {
        const { data } = await AuthRepository.login(email, password);
        commit('setAccessToken', data.data.tokens.access.token);
        commit('setRefreshToken', data.data.tokens.access.token);
        return true;
      } catch (err) {
        commit('ui/showAlert', 'Invalid email or password', { root: true });
        return false;
      }
    }
  }
};
