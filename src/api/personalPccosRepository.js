import querystring from 'querystring';
import api from './API';

const PATH = 'personal-pccos';

export default {
  get: (id, token) => api.token(token)
    .get(`/${PATH}/${id}`),
  list: query => api.noAuth()
    .get(`/${PATH}?${querystring.stringify(query)}`),
  update: (id, body, token) => api.token(token)
    .put(`/${PATH}/${id}`, body),
  save: (body, token) => api.token(token)
    .post(`/${PATH}`, body),
};
