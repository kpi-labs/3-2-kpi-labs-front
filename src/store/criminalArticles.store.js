import all from '../api';
import { normalizeServerErrorResponse } from '../helpers';

const { CriminalArticlesRepository } = all;

export default {
  namespaced: true,
  state: {
    articles: []
  },
  mutations: {
    setArticles(state, articles) {
      state.articles = articles;
    }
  },
  actions: {
    async loadAll({ commit }) {
      commit('ui/startLoading', null, { root: true });
      try {
        const { data } = await CriminalArticlesRepository.list();
        commit('setArticles', data.data);
      } catch (err) {
        commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
      }
      commit('ui/stopLoading', null, { root: true });
    }
  }
};
