import axios from 'axios';

const baseURL = 'http://localhost:3000/api/v1';

export default {
  basic: (username, password) => axios.create({
    baseURL,
    auth: {
      username,
      password,
    },
  }),
  token(token) {
    const config = { baseURL };
    config.headers = { Authorization: `Bearer ${token}` };
    return axios.create(config);
  },
  noAuth: () => axios.create({ baseURL })
};
