import querystring from 'querystring';
import api from './API';

const PATH = 'pccos-updates-logs';

export default {
  list: query => api.noAuth()
    .get(`/${PATH}?${querystring.stringify(query)}`),
};
