import api from './API';

const PATH = 'criminal-articles';

export default {
  list: () => api.noAuth()
    .get(`/${PATH}`),
};
