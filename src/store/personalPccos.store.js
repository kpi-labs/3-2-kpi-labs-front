import all from '../api';
import { normalizeServerErrorResponse } from '../helpers';

const { PersonalPccosRepository } = all;

export default {
  namespaced: true,
  state: {
    personalPccos: [],
    personalPcco: null,
    limit: 20,
    skip: 0,
    total: 0,
    totalPages: 0
  },
  mutations: {
    clearOne(state) {
      state.personalPcco = null;
    },
    setPcco(state, pcco) {
      state.personalPcco = pcco;
    },
    setPersonalPccosList(state, { data, meta: { total, skip, limit } }) {
      state.personalPccos = data;
      state.total = total;
      state.skip = skip;
      state.limit = limit;
      state.totalPages = Math.ceil(total / limit);
    },
    clearMany(state) {
      state.personalPccos = [];
      state.total = 0;
      state.skip = 0;
    }
  },
  actions: {
    async loadOne({ commit, rootState }, id) {
      commit('ui/startLoading', null, { root: true });
      try {
        const { data: { data: pcco } } = await PersonalPccosRepository.get(id, rootState.auth.accessToken);
        commit('ui/stopLoading', null, { root: true });
        commit('setPcco', pcco);
        return true;
      } catch (err) {
        commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
        commit('ui/stopLoading', null, { root: true });
        return false;
      }
    },
    async loadMany({ commit }, query) {
      commit('ui/startLoading', null, { root: true });
      PersonalPccosRepository.list(query)
        .then(({ data }) => {
          commit('ui/stopLoading', null, { root: true });
          commit('setPersonalPccosList', data);
        })
        .catch((err) => {
          commit('ui/stopLoading', null, { root: true });
          commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
        });
    },
    async update({ commit, rootState }, { id, body }) {
      commit('ui/startLoading', null, { root: true });
      try {
        await PersonalPccosRepository.update(id, body, rootState.auth.accessToken);
        return true;
      } catch (err) {
        commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
        return false;
      } finally {
        commit('ui/stopLoading', null, { root: true });
      }
    },
    async save({ commit, rootState }, { body }) {
      commit('ui/startLoading', null, { root: true });
      try {
        await PersonalPccosRepository.save(body, rootState.auth.accessToken);
        return true;
      } catch (err) {
        commit('ui/showAlert', normalizeServerErrorResponse(err), { root: true });
        return false;
      } finally {
        commit('ui/stopLoading', null, { root: true });
      }
    },
    clearMany({ commit }) {
      commit('clearMany');
    },
    clearOne({ commit }) {
      commit('clearOne');
    }
  }
};
