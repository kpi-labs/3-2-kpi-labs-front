import api from './API';

const PATH = 'auth';

export default {
  login: (email, password) => api.basic(email, password)
    .post(`/${PATH}/login`)
};
