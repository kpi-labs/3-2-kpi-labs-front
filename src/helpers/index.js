export function normalizeServerErrorResponse(error) {
  if (error.response && error.response.data) {
    const { data } = error.response;
    if (data.message) {
      return data.message;
    }
    if (error.response.status === 423) {
      return Object.values(data.data)
        .join('\n');
    }
    if (data.data) {
      return data.data;
    }
  }
  return error.toString();
}
